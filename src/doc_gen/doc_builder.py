import os, time

output_html = '''<html>
<head>
  <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.0.3/styles/default.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.0.3/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>
</head>
<body>
<script src="https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js"></script>
        <script>
            mermaid.initialize({ startOnLoad: true });
        </script>'''

print(os.getcwd())
for filename in sorted(os.listdir("src/examples")):
    if filename.endswith(".py") and not filename.startswith('_'):
        print('running exemple script: ', filename)
        os.system("python3 src/examples/" + filename)
        time.sleep(1)
        with open("src/examples/" + filename, 'r') as f:
            title = filename.replace('.py','').replace('_',' ')
            title = "Example " + title[0] + " -" + title[1:] 
            output_html += "<h3>" + title +":</h3>\n"
            output_html += '<pre><code class="python">'
            started = False
            for line in f.readlines():
                if started:
                    output_html += line
                else:
                    if line.strip() == "#"*5:
                        started = True
            output_html += '</code></pre>'
        example_name = os.path.basename(filename).replace('.py','')
        for example in sorted(os.listdir("public/examples")):
            if example.startswith(example_name):
                with open("public/examples/" + example, 'r') as f:
                    output_html += f.read()
                    
output_html += '</body></html>'

with open("public/examples/all.html", "w") as text_file:
    text_file.write(output_html)
                