import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

cnt = 0

def example_filepath():
    global cnt
    cnt += 1
    return "public/examples/" + os.path.basename( __file__.replace('.py',str(cnt) + '.txt')) 

#####
from odysseus.odysseus import Odysseus

class Linked_list:
        def __init__(self, value = 0, parent = None):
            self.child = None
            self.value = value
            if parent:
                parent.child = self
    
n0 = Linked_list(0)
n1 = Linked_list(1,n0)
n2 = Linked_list(2,n1)
n3 = Linked_list(3,n2)
n4 = Linked_list(4,n3)

odys = Odysseus()
odys.new_mermaid(n0)
odys.dump_to_file(example_filepath(), 'Simple linked list example')

#adding loop in a list:
n4.child = n1
odys.new_mermaid(n0)
odys.dump_to_file(example_filepath(), 'Same list with a loop')

#starting inspection from a different node
odys.new_mermaid(n3)
odys.dump_to_file(example_filepath(), 'Same list, but starting from 3rd node - previous nodes are lost')
