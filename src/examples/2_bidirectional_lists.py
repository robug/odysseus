import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

cnt = 0

def example_filepath():
    global cnt
    cnt += 1
    return "public/examples/" + os.path.basename( __file__.replace('.py',str(cnt) + '.txt')) 

#####
from odysseus.odysseus import Odysseus

class Linked_list:
        def __init__(self, value = 0, parent = None):
            self.child = None
            self.value = value
            if parent:
                parent.child = self
    
class Bidirect_list(Linked_list):
    def __init__(self, value = 0, parent = None):
        super().__init__(value, parent)
        self.parent = parent
        
b0 = Bidirect_list(0)
b1 = Bidirect_list(1,b0)
b2 = Bidirect_list(2,b1)
b3 = Bidirect_list(3,b2)
b4 = Bidirect_list(4,b3)
b5 = Bidirect_list(5,b4)

odys = Odysseus()
odys.new_mermaid(b0)
odys.dump_to_file(example_filepath(), 'Regular bidirectional list.')

#this one can be started from an arbitrary node
odys.new_mermaid(b2)
odys.dump_to_file(example_filepath(), 'Same list, but starting with a different node. No information loss for bidircetional lists.')
