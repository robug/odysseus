import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

cnt = 0

def example_filepath():
    global cnt
    cnt += 1
    return "public/examples/" + os.path.basename( __file__.replace('.py',str(cnt) + '.txt')) 

#####
from odysseus.odysseus import Odysseus

class Linked_list:
        def __init__(self, value = 0, parent = None):
            self.child = None
            self.value = value
            if parent:
                parent.child = self
    
class Bidirect_list(Linked_list):
    def __init__(self, value = 0, parent = None):
        super().__init__(value, parent)
        self.parent = parent
        
n0 = Linked_list(0)
n1 = Linked_list(1,n0)
n2 = Linked_list(2,n1)
n3 = Linked_list(3,n2)
n4 = Linked_list(4,n3)

odys = Odysseus()
odys.new_mermaid(n0)
odys.dump_to_file(example_filepath(), 'Visualizing one simple branch')

#creating new branch
m0 = Linked_list(10)
m1 = Linked_list(11,m0)
m2 = Linked_list(12,m1)
#linking with old one
m2.child = n2

odys.add_mermaid(m0)
odys.dump_to_file(example_filepath(), 'Adding additional object detects its connection to existing ones - a branch is added')

#another branch
m3 = Bidirect_list(13)
m4 = Bidirect_list(14,m3)
#linking to previous
m3.parent = n3

odys.add_mermaid(m4)
odys.dump_to_file(example_filepath(), 'The same for this bidirectional branch')