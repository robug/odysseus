import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

cnt = 0

def example_filepath():
    global cnt
    cnt += 1
    return "public/examples/" + os.path.basename( __file__.replace('.py',str(cnt) + '.txt'))


#####
from odysseus.odysseus import Odysseus
    
class A:
    def __init__(self, val = 'A'):
        self.val = val
        self.tp = 'A'
        self.some_ref = None

class B:
    def __init__(self, val = 'B'):
        self.val = val
        self.tp = 'B'
        self.some_ref = None
        
class C:
    def __init__(self, val = 'C'):
        self.val = val
        self.tp = 'C'
        self.some_ref = None
        
a1 = A('a1')
b1 = B('b1')
c1 = C('c1')

a1.some_ref = b1
b1.some_ref = c1
c1.some_ref = a1

odys = Odysseus()
odys.new_mermaid(a1)
odys.dump_to_file(example_filepath(), 'Unrelated objects - without a common ancestor')