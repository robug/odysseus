import types
import sys

class Object_parser:
    @staticmethod
    def strip_relatives(input):
        relations = []
        fields = []
        for attr, val in input.__dict__.items():
            if isinstance(val, type(input)) or isinstance(input,type(val)):
                relations.append(attr)
            else:
                if val is not None:
                    fields.append(attr)
        return fields, relations
    
    @staticmethod
    def strip_all(input):
        relations = []
        fields = []
        for attr, val in input.__dict__.items():
            if hasattr(val,'__dict__'):
                relations.append(attr)
            else:
                if val is not None:
                    fields.append(attr)
        return fields, relations

    @staticmethod
    def get_objects(input):
        # for name, instance in input:
        #     print(name,repr(instance))
        
        for name, instance in input:
            if not name.startswith('__') and not type(instance).__name__ in ['type', 'module']:
                print (name , type(instance).__name__)
        
class Mermaid_parser:
    @staticmethod
    def get_tag(input):
        return str(id(input))
    
    def __init__(self, show_id = True):
        self.text = ""
        self.show_id = show_id
        
    def clear(self):
        self.text = ""
        
    def get_graph_start():
        return "graph LR;"
    
    def get_md_header():
        return "```mermaid\n" + Mermaid_parser.get_graph_start()
    
    def get_md_footer():
        return '\n```'
    
    def get_html_header():
        return '''<html>
    <body>
        <script src="https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js"></script>
        <script>
            mermaid.initialize({ startOnLoad: true });
        </script>
        <div class="mermaid">
        ''' + '\n' + Mermaid_parser.get_graph_start()    
    def get_html_footer():
        return '''
        </div>
    </body>
</html>
'''
    
    def set_parameters(self, show_id = True):
        self.show_id = show_id
        self.clear()
    
    def get_markdown(self, title: str = None):
        title = "" #"" if not title else ( title + '\n')
        return Mermaid_parser.get_md_header() + title + self.text + Mermaid_parser.get_md_footer() 
    
    def get_html(self, title: str = None):
        title = "" #"" if not title else ( title + '\n')
        return Mermaid_parser.get_html_header() + title + self.text + Mermaid_parser.get_html_footer()
    
    def get_pure(self, title: str = None):
        title = "" #"" if not title else ( title + '\n')
        return Mermaid_parser.get_graph_start() + title + self.text
    
    def get_html_section(self, title: str = None):
        return (f"<h4>{title}</h4>\n" if title else "") + '<div class="mermaid">' + Mermaid_parser.get_graph_start() + self.text + '</div>'
 
    def get_label(self, input):
        input_id = id(input)
        label_lines = []
        values, _ = Object_parser.strip_all(input)
        if len(values) == 1:
            label_lines.append( str( getattr(input,values[0]) ) )
        elif len(values) > 1:
            for attr in values:
                value = getattr(input, attr)
                label_lines.append(f"{attr}:{value}")
        if self.show_id or not label_lines:
            label_lines.append(f"#{input_id}")
        return "<br>".join(label_lines)
    
    def add_object(self, input):
        tag = self.get_tag(input)
        label = self.get_label(input)
        self.text += f"\n{tag}({label})"

    def add_relation(self, *objects):
        self.text += '\n' + '-->'.join( map( self.get_tag, objects ) )

class Odysseus:
    def __init__(self):
        self.processed = set()
        self.mermaid = Mermaid_parser()
        
    def new_mermaid(self, input, show_id = False):        
        self.processed = set()
        self.mermaid.set_parameters(show_id = show_id)
        self.process(input)
    
    def add_mermaid(self, input):
        self.process(input)    
    
    def dump_to_file(self, filename: str, title = None):
        if filename.endswith('.html'):
            fcn = self.mermaid.get_html
        elif filename.endswith('.txt'):
            fcn = self.mermaid.get_html_section
        else:
            fcn = self.mermaid.get_markdown
        
        with open(filename, "w") as text_file:
            text_file.write(fcn(title))            

    def process(self, input):        
        if input in self.processed:
            return
        _, relations = Object_parser.strip_all(input)
        self.mermaid.add_object(input)
        self.processed.add(input)
        for relation in relations:
            next = getattr(input, relation)
            self.mermaid.add_relation(input, next)
            self.process(next)

instance = Odysseus()

#Object_parser.get_objects( list(locals().items()) )
   

#if __name__ == "__main__":

# dicts and lists as llinked lists or as a subgraphs